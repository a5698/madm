package br.com.esmeralda.sca.domain;

import br.com.esmeralda.sca.domain.enumeration.TipoCategoria;
import br.com.esmeralda.sca.domain.enumeration.TipoEstadoConservacao;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Equipamento.
 */
@Entity
@Table(name = "equipamento")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Equipamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "numero_patrimonio", nullable = false)
    private String numeroPatrimonio;

    @NotNull
    @Column(name = "numero_serie", nullable = false)
    private String numeroSerie;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "categoria", nullable = false)
    private TipoCategoria categoria;

    @NotNull
    @Column(name = "fabricante", nullable = false)
    private String fabricante;

    @NotNull
    @Column(name = "modelo", nullable = false)
    private String modelo;

    @Column(name = "data_manutencao")
    private LocalDate dataManutencao;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado_conservacao")
    private TipoEstadoConservacao estadoConservacao;

    @Column(name = "funcionando")
    private Boolean funcionando;

    @JsonIgnoreProperties(value = { "equipamento", "insumo" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Responsavel responsavel;

    @OneToMany(mappedBy = "equipamento")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "equipamento" }, allowSetters = true)
    private Set<Servico> servicos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Equipamento id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroPatrimonio() {
        return this.numeroPatrimonio;
    }

    public Equipamento numeroPatrimonio(String numeroPatrimonio) {
        this.setNumeroPatrimonio(numeroPatrimonio);
        return this;
    }

    public void setNumeroPatrimonio(String numeroPatrimonio) {
        this.numeroPatrimonio = numeroPatrimonio;
    }

    public String getNumeroSerie() {
        return this.numeroSerie;
    }

    public Equipamento numeroSerie(String numeroSerie) {
        this.setNumeroSerie(numeroSerie);
        return this;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public TipoCategoria getCategoria() {
        return this.categoria;
    }

    public Equipamento categoria(TipoCategoria categoria) {
        this.setCategoria(categoria);
        return this;
    }

    public void setCategoria(TipoCategoria categoria) {
        this.categoria = categoria;
    }

    public String getFabricante() {
        return this.fabricante;
    }

    public Equipamento fabricante(String fabricante) {
        this.setFabricante(fabricante);
        return this;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return this.modelo;
    }

    public Equipamento modelo(String modelo) {
        this.setModelo(modelo);
        return this;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public LocalDate getDataManutencao() {
        return this.dataManutencao;
    }

    public Equipamento dataManutencao(LocalDate dataManutencao) {
        this.setDataManutencao(dataManutencao);
        return this;
    }

    public void setDataManutencao(LocalDate dataManutencao) {
        this.dataManutencao = dataManutencao;
    }

    public TipoEstadoConservacao getEstadoConservacao() {
        return this.estadoConservacao;
    }

    public Equipamento estadoConservacao(TipoEstadoConservacao estadoConservacao) {
        this.setEstadoConservacao(estadoConservacao);
        return this;
    }

    public void setEstadoConservacao(TipoEstadoConservacao estadoConservacao) {
        this.estadoConservacao = estadoConservacao;
    }

    public Boolean getFuncionando() {
        return this.funcionando;
    }

    public Equipamento funcionando(Boolean funcionando) {
        this.setFuncionando(funcionando);
        return this;
    }

    public void setFuncionando(Boolean funcionando) {
        this.funcionando = funcionando;
    }

    public Responsavel getResponsavel() {
        return this.responsavel;
    }

    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }

    public Equipamento responsavel(Responsavel responsavel) {
        this.setResponsavel(responsavel);
        return this;
    }

    public Set<Servico> getServicos() {
        return this.servicos;
    }

    public void setServicos(Set<Servico> servicos) {
        if (this.servicos != null) {
            this.servicos.forEach(i -> i.setEquipamento(null));
        }
        if (servicos != null) {
            servicos.forEach(i -> i.setEquipamento(this));
        }
        this.servicos = servicos;
    }

    public Equipamento servicos(Set<Servico> servicos) {
        this.setServicos(servicos);
        return this;
    }

    public Equipamento addServico(Servico servico) {
        this.servicos.add(servico);
        servico.setEquipamento(this);
        return this;
    }

    public Equipamento removeServico(Servico servico) {
        this.servicos.remove(servico);
        servico.setEquipamento(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Equipamento)) {
            return false;
        }
        return id != null && id.equals(((Equipamento) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Equipamento{" +
            "id=" + getId() +
            ", numeroPatrimonio='" + getNumeroPatrimonio() + "'" +
            ", numeroSerie='" + getNumeroSerie() + "'" +
            ", categoria='" + getCategoria() + "'" +
            ", fabricante='" + getFabricante() + "'" +
            ", modelo='" + getModelo() + "'" +
            ", dataManutencao='" + getDataManutencao() + "'" +
            ", estadoConservacao='" + getEstadoConservacao() + "'" +
            ", funcionando='" + getFuncionando() + "'" +
            "}";
    }
}
