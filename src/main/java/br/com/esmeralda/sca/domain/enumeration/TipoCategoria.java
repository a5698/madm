package br.com.esmeralda.sca.domain.enumeration;

/**
 * The TipoCategoria enumeration.
 */
public enum TipoCategoria {
    MAQUINA_PESADA,
    COMPUTADOR,
    MOBILIA,
    MOTOR,
}
