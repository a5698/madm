package br.com.esmeralda.sca.domain.enumeration;

/**
 * The TipoServico enumeration.
 */
public enum TipoServico {
    MANUTENCAO_PREVENTIVA,
    CONCERTO,
    TROCA_PECA,
}
