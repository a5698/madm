package br.com.esmeralda.sca.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Responsavel.
 */
@Entity
@Table(name = "responsavel")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Responsavel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nome_departamento", nullable = false)
    private String nomeDepartamento;

    @NotNull
    @Column(name = "utilizacao", nullable = false)
    private String utilizacao;

    @Column(name = "nome_responsavel")
    private String nomeResponsavel;

    @JsonIgnoreProperties(value = { "responsavel", "servicos" }, allowSetters = true)
    @OneToOne(mappedBy = "responsavel")
    private Equipamento equipamento;

    @JsonIgnoreProperties(value = { "responsavel" }, allowSetters = true)
    @OneToOne(mappedBy = "responsavel")
    private Insumo insumo;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Responsavel id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeDepartamento() {
        return this.nomeDepartamento;
    }

    public Responsavel nomeDepartamento(String nomeDepartamento) {
        this.setNomeDepartamento(nomeDepartamento);
        return this;
    }

    public void setNomeDepartamento(String nomeDepartamento) {
        this.nomeDepartamento = nomeDepartamento;
    }

    public String getUtilizacao() {
        return this.utilizacao;
    }

    public Responsavel utilizacao(String utilizacao) {
        this.setUtilizacao(utilizacao);
        return this;
    }

    public void setUtilizacao(String utilizacao) {
        this.utilizacao = utilizacao;
    }

    public String getNomeResponsavel() {
        return this.nomeResponsavel;
    }

    public Responsavel nomeResponsavel(String nomeResponsavel) {
        this.setNomeResponsavel(nomeResponsavel);
        return this;
    }

    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }

    public Equipamento getEquipamento() {
        return this.equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        if (this.equipamento != null) {
            this.equipamento.setResponsavel(null);
        }
        if (equipamento != null) {
            equipamento.setResponsavel(this);
        }
        this.equipamento = equipamento;
    }

    public Responsavel equipamento(Equipamento equipamento) {
        this.setEquipamento(equipamento);
        return this;
    }

    public Insumo getInsumo() {
        return this.insumo;
    }

    public void setInsumo(Insumo insumo) {
        if (this.insumo != null) {
            this.insumo.setResponsavel(null);
        }
        if (insumo != null) {
            insumo.setResponsavel(this);
        }
        this.insumo = insumo;
    }

    public Responsavel insumo(Insumo insumo) {
        this.setInsumo(insumo);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Responsavel)) {
            return false;
        }
        return id != null && id.equals(((Responsavel) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Responsavel{" +
            "id=" + getId() +
            ", nomeDepartamento='" + getNomeDepartamento() + "'" +
            ", utilizacao='" + getUtilizacao() + "'" +
            ", nomeResponsavel='" + getNomeResponsavel() + "'" +
            "}";
    }
}
