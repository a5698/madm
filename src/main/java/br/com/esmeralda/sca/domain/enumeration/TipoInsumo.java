package br.com.esmeralda.sca.domain.enumeration;

/**
 * The TipoInsumo enumeration.
 */
public enum TipoInsumo {
    ACIDO,
    ADJUVANTE_AGRICOLA,
    DEFENSIVO_AGRICOLA,
}
