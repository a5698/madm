package br.com.esmeralda.sca.domain;

import br.com.esmeralda.sca.domain.enumeration.TipoServico;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Servico.
 */
@Entity
@Table(name = "servico")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Servico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cod_servico", nullable = false)
    private String codServico;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_servico", nullable = false)
    private TipoServico tipoServico;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @NotNull
    @Column(name = "data_servico", nullable = false)
    private LocalDate dataServico;

    @ManyToOne
    @JsonIgnoreProperties(value = { "responsavel", "servicos" }, allowSetters = true)
    private Equipamento equipamento;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Servico id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodServico() {
        return this.codServico;
    }

    public Servico codServico(String codServico) {
        this.setCodServico(codServico);
        return this;
    }

    public void setCodServico(String codServico) {
        this.codServico = codServico;
    }

    public TipoServico getTipoServico() {
        return this.tipoServico;
    }

    public Servico tipoServico(TipoServico tipoServico) {
        this.setTipoServico(tipoServico);
        return this;
    }

    public void setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Servico descricao(String descricao) {
        this.setDescricao(descricao);
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataServico() {
        return this.dataServico;
    }

    public Servico dataServico(LocalDate dataServico) {
        this.setDataServico(dataServico);
        return this;
    }

    public void setDataServico(LocalDate dataServico) {
        this.dataServico = dataServico;
    }

    public Equipamento getEquipamento() {
        return this.equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public Servico equipamento(Equipamento equipamento) {
        this.setEquipamento(equipamento);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Servico)) {
            return false;
        }
        return id != null && id.equals(((Servico) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Servico{" +
            "id=" + getId() +
            ", codServico='" + getCodServico() + "'" +
            ", tipoServico='" + getTipoServico() + "'" +
            ", descricao='" + getDescricao() + "'" +
            ", dataServico='" + getDataServico() + "'" +
            "}";
    }
}
