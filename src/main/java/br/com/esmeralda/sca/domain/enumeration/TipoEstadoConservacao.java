package br.com.esmeralda.sca.domain.enumeration;

/**
 * The TipoEstadoConservacao enumeration.
 */
public enum TipoEstadoConservacao {
    NOVO,
    USADO_BOM_ESTADO,
    USADO_MEDIO_ESTADO,
    USADO_MAU_ESTADO,
    COM_DEFEITO,
}
