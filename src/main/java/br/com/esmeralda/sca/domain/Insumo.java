package br.com.esmeralda.sca.domain;

import br.com.esmeralda.sca.domain.enumeration.TipoInsumo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Insumo.
 */
@Entity
@Table(name = "insumo")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Insumo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cod_insumo", nullable = false)
    private String codInsumo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_insumo", nullable = false)
    private TipoInsumo tipoInsumo;

    @NotNull
    @Column(name = "fabricante", nullable = false)
    private String fabricante;

    @NotNull
    @Column(name = "modelo", nullable = false)
    private String modelo;

    @JsonIgnoreProperties(value = { "equipamento", "insumo" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Responsavel responsavel;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Insumo id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodInsumo() {
        return this.codInsumo;
    }

    public Insumo codInsumo(String codInsumo) {
        this.setCodInsumo(codInsumo);
        return this;
    }

    public void setCodInsumo(String codInsumo) {
        this.codInsumo = codInsumo;
    }

    public TipoInsumo getTipoInsumo() {
        return this.tipoInsumo;
    }

    public Insumo tipoInsumo(TipoInsumo tipoInsumo) {
        this.setTipoInsumo(tipoInsumo);
        return this;
    }

    public void setTipoInsumo(TipoInsumo tipoInsumo) {
        this.tipoInsumo = tipoInsumo;
    }

    public String getFabricante() {
        return this.fabricante;
    }

    public Insumo fabricante(String fabricante) {
        this.setFabricante(fabricante);
        return this;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return this.modelo;
    }

    public Insumo modelo(String modelo) {
        this.setModelo(modelo);
        return this;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Responsavel getResponsavel() {
        return this.responsavel;
    }

    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }

    public Insumo responsavel(Responsavel responsavel) {
        this.setResponsavel(responsavel);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Insumo)) {
            return false;
        }
        return id != null && id.equals(((Insumo) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Insumo{" +
            "id=" + getId() +
            ", codInsumo='" + getCodInsumo() + "'" +
            ", tipoInsumo='" + getTipoInsumo() + "'" +
            ", fabricante='" + getFabricante() + "'" +
            ", modelo='" + getModelo() + "'" +
            "}";
    }
}
