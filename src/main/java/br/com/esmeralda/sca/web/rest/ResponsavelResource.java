package br.com.esmeralda.sca.web.rest;

import br.com.esmeralda.sca.domain.Responsavel;
import br.com.esmeralda.sca.repository.ResponsavelRepository;
import br.com.esmeralda.sca.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.com.esmeralda.sca.domain.Responsavel}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ResponsavelResource {

    private final Logger log = LoggerFactory.getLogger(ResponsavelResource.class);

    private static final String ENTITY_NAME = "responsavel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResponsavelRepository responsavelRepository;

    public ResponsavelResource(ResponsavelRepository responsavelRepository) {
        this.responsavelRepository = responsavelRepository;
    }

    /**
     * {@code POST  /responsavels} : Create a new responsavel.
     *
     * @param responsavel the responsavel to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new responsavel, or with status {@code 400 (Bad Request)} if the responsavel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/responsavels")
    public ResponseEntity<Responsavel> createResponsavel(@Valid @RequestBody Responsavel responsavel) throws URISyntaxException {
        log.debug("REST request to save Responsavel : {}", responsavel);
        if (responsavel.getId() != null) {
            throw new BadRequestAlertException("A new responsavel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Responsavel result = responsavelRepository.save(responsavel);
        return ResponseEntity
            .created(new URI("/api/responsavels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /responsavels/:id} : Updates an existing responsavel.
     *
     * @param id the id of the responsavel to save.
     * @param responsavel the responsavel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated responsavel,
     * or with status {@code 400 (Bad Request)} if the responsavel is not valid,
     * or with status {@code 500 (Internal Server Error)} if the responsavel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/responsavels/{id}")
    public ResponseEntity<Responsavel> updateResponsavel(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Responsavel responsavel
    ) throws URISyntaxException {
        log.debug("REST request to update Responsavel : {}, {}", id, responsavel);
        if (responsavel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, responsavel.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!responsavelRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Responsavel result = responsavelRepository.save(responsavel);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, responsavel.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /responsavels/:id} : Partial updates given fields of an existing responsavel, field will ignore if it is null
     *
     * @param id the id of the responsavel to save.
     * @param responsavel the responsavel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated responsavel,
     * or with status {@code 400 (Bad Request)} if the responsavel is not valid,
     * or with status {@code 404 (Not Found)} if the responsavel is not found,
     * or with status {@code 500 (Internal Server Error)} if the responsavel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/responsavels/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Responsavel> partialUpdateResponsavel(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Responsavel responsavel
    ) throws URISyntaxException {
        log.debug("REST request to partial update Responsavel partially : {}, {}", id, responsavel);
        if (responsavel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, responsavel.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!responsavelRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Responsavel> result = responsavelRepository
            .findById(responsavel.getId())
            .map(existingResponsavel -> {
                if (responsavel.getNomeDepartamento() != null) {
                    existingResponsavel.setNomeDepartamento(responsavel.getNomeDepartamento());
                }
                if (responsavel.getUtilizacao() != null) {
                    existingResponsavel.setUtilizacao(responsavel.getUtilizacao());
                }
                if (responsavel.getNomeResponsavel() != null) {
                    existingResponsavel.setNomeResponsavel(responsavel.getNomeResponsavel());
                }

                return existingResponsavel;
            })
            .map(responsavelRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, responsavel.getId().toString())
        );
    }

    /**
     * {@code GET  /responsavels} : get all the responsavels.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of responsavels in body.
     */
    @GetMapping("/responsavels")
    public List<Responsavel> getAllResponsavels(@RequestParam(required = false) String filter) {
        if ("equipamento-is-null".equals(filter)) {
            log.debug("REST request to get all Responsavels where equipamento is null");
            return StreamSupport
                .stream(responsavelRepository.findAll().spliterator(), false)
                .filter(responsavel -> responsavel.getEquipamento() == null)
                .collect(Collectors.toList());
        }

        if ("insumo-is-null".equals(filter)) {
            log.debug("REST request to get all Responsavels where insumo is null");
            return StreamSupport
                .stream(responsavelRepository.findAll().spliterator(), false)
                .filter(responsavel -> responsavel.getInsumo() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all Responsavels");
        return responsavelRepository.findAll();
    }

    /**
     * {@code GET  /responsavels/:id} : get the "id" responsavel.
     *
     * @param id the id of the responsavel to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the responsavel, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/responsavels/{id}")
    public ResponseEntity<Responsavel> getResponsavel(@PathVariable Long id) {
        log.debug("REST request to get Responsavel : {}", id);
        Optional<Responsavel> responsavel = responsavelRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(responsavel);
    }

    /**
     * {@code DELETE  /responsavels/:id} : delete the "id" responsavel.
     *
     * @param id the id of the responsavel to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/responsavels/{id}")
    public ResponseEntity<Void> deleteResponsavel(@PathVariable Long id) {
        log.debug("REST request to delete Responsavel : {}", id);
        responsavelRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
