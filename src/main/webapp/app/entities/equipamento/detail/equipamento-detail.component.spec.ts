import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EquipamentoDetailComponent } from './equipamento-detail.component';

describe('Equipamento Management Detail Component', () => {
  let comp: EquipamentoDetailComponent;
  let fixture: ComponentFixture<EquipamentoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipamentoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ equipamento: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EquipamentoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EquipamentoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load equipamento on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.equipamento).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
