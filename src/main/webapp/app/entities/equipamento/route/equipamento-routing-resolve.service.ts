import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEquipamento, Equipamento } from '../equipamento.model';
import { EquipamentoService } from '../service/equipamento.service';

@Injectable({ providedIn: 'root' })
export class EquipamentoRoutingResolveService implements Resolve<IEquipamento> {
  constructor(protected service: EquipamentoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEquipamento> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((equipamento: HttpResponse<Equipamento>) => {
          if (equipamento.body) {
            return of(equipamento.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Equipamento());
  }
}
