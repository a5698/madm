jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IEquipamento, Equipamento } from '../equipamento.model';
import { EquipamentoService } from '../service/equipamento.service';

import { EquipamentoRoutingResolveService } from './equipamento-routing-resolve.service';

describe('Equipamento routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: EquipamentoRoutingResolveService;
  let service: EquipamentoService;
  let resultEquipamento: IEquipamento | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(EquipamentoRoutingResolveService);
    service = TestBed.inject(EquipamentoService);
    resultEquipamento = undefined;
  });

  describe('resolve', () => {
    it('should return IEquipamento returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEquipamento = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEquipamento).toEqual({ id: 123 });
    });

    it('should return new IEquipamento if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEquipamento = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultEquipamento).toEqual(new Equipamento());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Equipamento })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEquipamento = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEquipamento).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
