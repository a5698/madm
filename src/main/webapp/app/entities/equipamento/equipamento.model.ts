import * as dayjs from 'dayjs';
import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { IServico } from 'app/entities/servico/servico.model';
import { TipoCategoria } from 'app/entities/enumerations/tipo-categoria.model';
import { TipoEstadoConservacao } from 'app/entities/enumerations/tipo-estado-conservacao.model';

export interface IEquipamento {
  id?: number;
  numeroPatrimonio?: string;
  numeroSerie?: string;
  categoria?: TipoCategoria;
  fabricante?: string;
  modelo?: string;
  dataManutencao?: dayjs.Dayjs | null;
  estadoConservacao?: TipoEstadoConservacao | null;
  funcionando?: boolean | null;
  responsavel?: IResponsavel | null;
  servicos?: IServico[] | null;
}

export class Equipamento implements IEquipamento {
  constructor(
    public id?: number,
    public numeroPatrimonio?: string,
    public numeroSerie?: string,
    public categoria?: TipoCategoria,
    public fabricante?: string,
    public modelo?: string,
    public dataManutencao?: dayjs.Dayjs | null,
    public estadoConservacao?: TipoEstadoConservacao | null,
    public funcionando?: boolean | null,
    public responsavel?: IResponsavel | null,
    public servicos?: IServico[] | null
  ) {
    this.funcionando = this.funcionando ?? false;
  }
}

export function getEquipamentoIdentifier(equipamento: IEquipamento): number | undefined {
  return equipamento.id;
}
