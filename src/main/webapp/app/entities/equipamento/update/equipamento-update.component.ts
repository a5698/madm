import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IEquipamento, Equipamento } from '../equipamento.model';
import { EquipamentoService } from '../service/equipamento.service';
import { IResponsavel } from 'app/entities/responsavel/responsavel.model';
import { ResponsavelService } from 'app/entities/responsavel/service/responsavel.service';
import { TipoCategoria } from 'app/entities/enumerations/tipo-categoria.model';
import { TipoEstadoConservacao } from 'app/entities/enumerations/tipo-estado-conservacao.model';

@Component({
  selector: 'jhi-equipamento-update',
  templateUrl: './equipamento-update.component.html',
})
export class EquipamentoUpdateComponent implements OnInit {
  isSaving = false;
  tipoCategoriaValues = Object.keys(TipoCategoria);
  tipoEstadoConservacaoValues = Object.keys(TipoEstadoConservacao);

  responsavelsCollection: IResponsavel[] = [];

  editForm = this.fb.group({
    id: [],
    numeroPatrimonio: [null, [Validators.required]],
    numeroSerie: [null, [Validators.required]],
    categoria: [null, [Validators.required]],
    fabricante: [null, [Validators.required]],
    modelo: [null, [Validators.required]],
    dataManutencao: [],
    estadoConservacao: [],
    funcionando: [],
    responsavel: [],
  });

  constructor(
    protected equipamentoService: EquipamentoService,
    protected responsavelService: ResponsavelService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ equipamento }) => {
      this.updateForm(equipamento);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const equipamento = this.createFromForm();
    if (equipamento.id !== undefined) {
      this.subscribeToSaveResponse(this.equipamentoService.update(equipamento));
    } else {
      this.subscribeToSaveResponse(this.equipamentoService.create(equipamento));
    }
  }

  trackResponsavelById(index: number, item: IResponsavel): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEquipamento>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(equipamento: IEquipamento): void {
    this.editForm.patchValue({
      id: equipamento.id,
      numeroPatrimonio: equipamento.numeroPatrimonio,
      numeroSerie: equipamento.numeroSerie,
      categoria: equipamento.categoria,
      fabricante: equipamento.fabricante,
      modelo: equipamento.modelo,
      dataManutencao: equipamento.dataManutencao,
      estadoConservacao: equipamento.estadoConservacao,
      funcionando: equipamento.funcionando,
      responsavel: equipamento.responsavel,
    });

    this.responsavelsCollection = this.responsavelService.addResponsavelToCollectionIfMissing(
      this.responsavelsCollection,
      equipamento.responsavel
    );
  }

  protected loadRelationshipsOptions(): void {
    this.responsavelService
      .query({ filter: 'equipamento-is-null' })
      .pipe(map((res: HttpResponse<IResponsavel[]>) => res.body ?? []))
      .pipe(
        map((responsavels: IResponsavel[]) =>
          this.responsavelService.addResponsavelToCollectionIfMissing(responsavels, this.editForm.get('responsavel')!.value)
        )
      )
      .subscribe((responsavels: IResponsavel[]) => (this.responsavelsCollection = responsavels));
  }

  protected createFromForm(): IEquipamento {
    return {
      ...new Equipamento(),
      id: this.editForm.get(['id'])!.value,
      numeroPatrimonio: this.editForm.get(['numeroPatrimonio'])!.value,
      numeroSerie: this.editForm.get(['numeroSerie'])!.value,
      categoria: this.editForm.get(['categoria'])!.value,
      fabricante: this.editForm.get(['fabricante'])!.value,
      modelo: this.editForm.get(['modelo'])!.value,
      dataManutencao: this.editForm.get(['dataManutencao'])!.value,
      estadoConservacao: this.editForm.get(['estadoConservacao'])!.value,
      funcionando: this.editForm.get(['funcionando'])!.value,
      responsavel: this.editForm.get(['responsavel'])!.value,
    };
  }
}
