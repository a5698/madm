import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EquipamentoComponent } from './list/equipamento.component';
import { EquipamentoDetailComponent } from './detail/equipamento-detail.component';
import { EquipamentoUpdateComponent } from './update/equipamento-update.component';
import { EquipamentoDeleteDialogComponent } from './delete/equipamento-delete-dialog.component';
import { EquipamentoRoutingModule } from './route/equipamento-routing.module';

@NgModule({
  imports: [SharedModule, EquipamentoRoutingModule],
  declarations: [EquipamentoComponent, EquipamentoDetailComponent, EquipamentoUpdateComponent, EquipamentoDeleteDialogComponent],
  entryComponents: [EquipamentoDeleteDialogComponent],
})
export class EquipamentoModule {}
