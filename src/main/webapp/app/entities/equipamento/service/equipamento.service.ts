import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEquipamento, getEquipamentoIdentifier } from '../equipamento.model';

export type EntityResponseType = HttpResponse<IEquipamento>;
export type EntityArrayResponseType = HttpResponse<IEquipamento[]>;

@Injectable({ providedIn: 'root' })
export class EquipamentoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/equipamentos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(equipamento: IEquipamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(equipamento);
    return this.http
      .post<IEquipamento>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(equipamento: IEquipamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(equipamento);
    return this.http
      .put<IEquipamento>(`${this.resourceUrl}/${getEquipamentoIdentifier(equipamento) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(equipamento: IEquipamento): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(equipamento);
    return this.http
      .patch<IEquipamento>(`${this.resourceUrl}/${getEquipamentoIdentifier(equipamento) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEquipamento>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEquipamento[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEquipamentoToCollectionIfMissing(
    equipamentoCollection: IEquipamento[],
    ...equipamentosToCheck: (IEquipamento | null | undefined)[]
  ): IEquipamento[] {
    const equipamentos: IEquipamento[] = equipamentosToCheck.filter(isPresent);
    if (equipamentos.length > 0) {
      const equipamentoCollectionIdentifiers = equipamentoCollection.map(equipamentoItem => getEquipamentoIdentifier(equipamentoItem)!);
      const equipamentosToAdd = equipamentos.filter(equipamentoItem => {
        const equipamentoIdentifier = getEquipamentoIdentifier(equipamentoItem);
        if (equipamentoIdentifier == null || equipamentoCollectionIdentifiers.includes(equipamentoIdentifier)) {
          return false;
        }
        equipamentoCollectionIdentifiers.push(equipamentoIdentifier);
        return true;
      });
      return [...equipamentosToAdd, ...equipamentoCollection];
    }
    return equipamentoCollection;
  }

  protected convertDateFromClient(equipamento: IEquipamento): IEquipamento {
    return Object.assign({}, equipamento, {
      dataManutencao: equipamento.dataManutencao?.isValid() ? equipamento.dataManutencao.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataManutencao = res.body.dataManutencao ? dayjs(res.body.dataManutencao) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((equipamento: IEquipamento) => {
        equipamento.dataManutencao = equipamento.dataManutencao ? dayjs(equipamento.dataManutencao) : undefined;
      });
    }
    return res;
  }
}
