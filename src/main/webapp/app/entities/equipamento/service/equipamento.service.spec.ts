import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { TipoCategoria } from 'app/entities/enumerations/tipo-categoria.model';
import { TipoEstadoConservacao } from 'app/entities/enumerations/tipo-estado-conservacao.model';
import { IEquipamento, Equipamento } from '../equipamento.model';

import { EquipamentoService } from './equipamento.service';

describe('Equipamento Service', () => {
  let service: EquipamentoService;
  let httpMock: HttpTestingController;
  let elemDefault: IEquipamento;
  let expectedResult: IEquipamento | IEquipamento[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EquipamentoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      numeroPatrimonio: 'AAAAAAA',
      numeroSerie: 'AAAAAAA',
      categoria: TipoCategoria.MAQUINA_PESADA,
      fabricante: 'AAAAAAA',
      modelo: 'AAAAAAA',
      dataManutencao: currentDate,
      estadoConservacao: TipoEstadoConservacao.NOVO,
      funcionando: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dataManutencao: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Equipamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dataManutencao: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataManutencao: currentDate,
        },
        returnedFromService
      );

      service.create(new Equipamento()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Equipamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          numeroPatrimonio: 'BBBBBB',
          numeroSerie: 'BBBBBB',
          categoria: 'BBBBBB',
          fabricante: 'BBBBBB',
          modelo: 'BBBBBB',
          dataManutencao: currentDate.format(DATE_FORMAT),
          estadoConservacao: 'BBBBBB',
          funcionando: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataManutencao: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Equipamento', () => {
      const patchObject = Object.assign(
        {
          numeroPatrimonio: 'BBBBBB',
          categoria: 'BBBBBB',
          fabricante: 'BBBBBB',
          modelo: 'BBBBBB',
          dataManutencao: currentDate.format(DATE_FORMAT),
          estadoConservacao: 'BBBBBB',
          funcionando: true,
        },
        new Equipamento()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dataManutencao: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Equipamento', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          numeroPatrimonio: 'BBBBBB',
          numeroSerie: 'BBBBBB',
          categoria: 'BBBBBB',
          fabricante: 'BBBBBB',
          modelo: 'BBBBBB',
          dataManutencao: currentDate.format(DATE_FORMAT),
          estadoConservacao: 'BBBBBB',
          funcionando: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dataManutencao: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Equipamento', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEquipamentoToCollectionIfMissing', () => {
      it('should add a Equipamento to an empty array', () => {
        const equipamento: IEquipamento = { id: 123 };
        expectedResult = service.addEquipamentoToCollectionIfMissing([], equipamento);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(equipamento);
      });

      it('should not add a Equipamento to an array that contains it', () => {
        const equipamento: IEquipamento = { id: 123 };
        const equipamentoCollection: IEquipamento[] = [
          {
            ...equipamento,
          },
          { id: 456 },
        ];
        expectedResult = service.addEquipamentoToCollectionIfMissing(equipamentoCollection, equipamento);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Equipamento to an array that doesn't contain it", () => {
        const equipamento: IEquipamento = { id: 123 };
        const equipamentoCollection: IEquipamento[] = [{ id: 456 }];
        expectedResult = service.addEquipamentoToCollectionIfMissing(equipamentoCollection, equipamento);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(equipamento);
      });

      it('should add only unique Equipamento to an array', () => {
        const equipamentoArray: IEquipamento[] = [{ id: 123 }, { id: 456 }, { id: 72112 }];
        const equipamentoCollection: IEquipamento[] = [{ id: 123 }];
        expectedResult = service.addEquipamentoToCollectionIfMissing(equipamentoCollection, ...equipamentoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const equipamento: IEquipamento = { id: 123 };
        const equipamento2: IEquipamento = { id: 456 };
        expectedResult = service.addEquipamentoToCollectionIfMissing([], equipamento, equipamento2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(equipamento);
        expect(expectedResult).toContain(equipamento2);
      });

      it('should accept null and undefined values', () => {
        const equipamento: IEquipamento = { id: 123 };
        expectedResult = service.addEquipamentoToCollectionIfMissing([], null, equipamento, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(equipamento);
      });

      it('should return initial array if no Equipamento is added', () => {
        const equipamentoCollection: IEquipamento[] = [{ id: 123 }];
        expectedResult = service.addEquipamentoToCollectionIfMissing(equipamentoCollection, undefined, null);
        expect(expectedResult).toEqual(equipamentoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
