import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IServico } from '../servico.model';
import { ServicoService } from '../service/servico.service';
import { ServicoDeleteDialogComponent } from '../delete/servico-delete-dialog.component';

@Component({
  selector: 'jhi-servico',
  templateUrl: './servico.component.html',
})
export class ServicoComponent implements OnInit {
  servicos?: IServico[];
  isLoading = false;

  constructor(protected servicoService: ServicoService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.servicoService.query().subscribe(
      (res: HttpResponse<IServico[]>) => {
        this.isLoading = false;
        this.servicos = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IServico): number {
    return item.id!;
  }

  delete(servico: IServico): void {
    const modalRef = this.modalService.open(ServicoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.servico = servico;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
