import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IServico, getServicoIdentifier } from '../servico.model';

export type EntityResponseType = HttpResponse<IServico>;
export type EntityArrayResponseType = HttpResponse<IServico[]>;

@Injectable({ providedIn: 'root' })
export class ServicoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/servicos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(servico: IServico): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(servico);
    return this.http
      .post<IServico>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(servico: IServico): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(servico);
    return this.http
      .put<IServico>(`${this.resourceUrl}/${getServicoIdentifier(servico) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(servico: IServico): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(servico);
    return this.http
      .patch<IServico>(`${this.resourceUrl}/${getServicoIdentifier(servico) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IServico>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IServico[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addServicoToCollectionIfMissing(servicoCollection: IServico[], ...servicosToCheck: (IServico | null | undefined)[]): IServico[] {
    const servicos: IServico[] = servicosToCheck.filter(isPresent);
    if (servicos.length > 0) {
      const servicoCollectionIdentifiers = servicoCollection.map(servicoItem => getServicoIdentifier(servicoItem)!);
      const servicosToAdd = servicos.filter(servicoItem => {
        const servicoIdentifier = getServicoIdentifier(servicoItem);
        if (servicoIdentifier == null || servicoCollectionIdentifiers.includes(servicoIdentifier)) {
          return false;
        }
        servicoCollectionIdentifiers.push(servicoIdentifier);
        return true;
      });
      return [...servicosToAdd, ...servicoCollection];
    }
    return servicoCollection;
  }

  protected convertDateFromClient(servico: IServico): IServico {
    return Object.assign({}, servico, {
      dataServico: servico.dataServico?.isValid() ? servico.dataServico.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dataServico = res.body.dataServico ? dayjs(res.body.dataServico) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((servico: IServico) => {
        servico.dataServico = servico.dataServico ? dayjs(servico.dataServico) : undefined;
      });
    }
    return res;
  }
}
