import * as dayjs from 'dayjs';
import { IEquipamento } from 'app/entities/equipamento/equipamento.model';
import { TipoServico } from 'app/entities/enumerations/tipo-servico.model';

export interface IServico {
  id?: number;
  codServico?: string;
  tipoServico?: TipoServico;
  descricao?: string;
  dataServico?: dayjs.Dayjs;
  equipamento?: IEquipamento | null;
}

export class Servico implements IServico {
  constructor(
    public id?: number,
    public codServico?: string,
    public tipoServico?: TipoServico,
    public descricao?: string,
    public dataServico?: dayjs.Dayjs,
    public equipamento?: IEquipamento | null
  ) {}
}

export function getServicoIdentifier(servico: IServico): number | undefined {
  return servico.id;
}
