import { IEquipamento } from 'app/entities/equipamento/equipamento.model';
import { IInsumo } from 'app/entities/insumo/insumo.model';

export interface IResponsavel {
  id?: number;
  nomeDepartamento?: string;
  utilizacao?: string;
  nomeResponsavel?: string | null;
  equipamento?: IEquipamento | null;
  insumo?: IInsumo | null;
}

export class Responsavel implements IResponsavel {
  constructor(
    public id?: number,
    public nomeDepartamento?: string,
    public utilizacao?: string,
    public nomeResponsavel?: string | null,
    public equipamento?: IEquipamento | null,
    public insumo?: IInsumo | null
  ) {}
}

export function getResponsavelIdentifier(responsavel: IResponsavel): number | undefined {
  return responsavel.id;
}
