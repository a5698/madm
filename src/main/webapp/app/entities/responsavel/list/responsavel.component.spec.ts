import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ResponsavelService } from '../service/responsavel.service';

import { ResponsavelComponent } from './responsavel.component';

describe('Responsavel Management Component', () => {
  let comp: ResponsavelComponent;
  let fixture: ComponentFixture<ResponsavelComponent>;
  let service: ResponsavelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ResponsavelComponent],
    })
      .overrideTemplate(ResponsavelComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ResponsavelComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ResponsavelService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.responsavels?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
