package br.com.esmeralda.sca.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.esmeralda.sca.IntegrationTest;
import br.com.esmeralda.sca.domain.Equipamento;
import br.com.esmeralda.sca.domain.enumeration.TipoCategoria;
import br.com.esmeralda.sca.domain.enumeration.TipoEstadoConservacao;
import br.com.esmeralda.sca.repository.EquipamentoRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EquipamentoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EquipamentoResourceIT {

    private static final String DEFAULT_NUMERO_PATRIMONIO = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_PATRIMONIO = "BBBBBBBBBB";

    private static final String DEFAULT_NUMERO_SERIE = "AAAAAAAAAA";
    private static final String UPDATED_NUMERO_SERIE = "BBBBBBBBBB";

    private static final TipoCategoria DEFAULT_CATEGORIA = TipoCategoria.MAQUINA_PESADA;
    private static final TipoCategoria UPDATED_CATEGORIA = TipoCategoria.COMPUTADOR;

    private static final String DEFAULT_FABRICANTE = "AAAAAAAAAA";
    private static final String UPDATED_FABRICANTE = "BBBBBBBBBB";

    private static final String DEFAULT_MODELO = "AAAAAAAAAA";
    private static final String UPDATED_MODELO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_MANUTENCAO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_MANUTENCAO = LocalDate.now(ZoneId.systemDefault());

    private static final TipoEstadoConservacao DEFAULT_ESTADO_CONSERVACAO = TipoEstadoConservacao.NOVO;
    private static final TipoEstadoConservacao UPDATED_ESTADO_CONSERVACAO = TipoEstadoConservacao.USADO_BOM_ESTADO;

    private static final Boolean DEFAULT_FUNCIONANDO = false;
    private static final Boolean UPDATED_FUNCIONANDO = true;

    private static final String ENTITY_API_URL = "/api/equipamentos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EquipamentoRepository equipamentoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEquipamentoMockMvc;

    private Equipamento equipamento;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipamento createEntity(EntityManager em) {
        Equipamento equipamento = new Equipamento()
            .numeroPatrimonio(DEFAULT_NUMERO_PATRIMONIO)
            .numeroSerie(DEFAULT_NUMERO_SERIE)
            .categoria(DEFAULT_CATEGORIA)
            .fabricante(DEFAULT_FABRICANTE)
            .modelo(DEFAULT_MODELO)
            .dataManutencao(DEFAULT_DATA_MANUTENCAO)
            .estadoConservacao(DEFAULT_ESTADO_CONSERVACAO)
            .funcionando(DEFAULT_FUNCIONANDO);
        return equipamento;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Equipamento createUpdatedEntity(EntityManager em) {
        Equipamento equipamento = new Equipamento()
            .numeroPatrimonio(UPDATED_NUMERO_PATRIMONIO)
            .numeroSerie(UPDATED_NUMERO_SERIE)
            .categoria(UPDATED_CATEGORIA)
            .fabricante(UPDATED_FABRICANTE)
            .modelo(UPDATED_MODELO)
            .dataManutencao(UPDATED_DATA_MANUTENCAO)
            .estadoConservacao(UPDATED_ESTADO_CONSERVACAO)
            .funcionando(UPDATED_FUNCIONANDO);
        return equipamento;
    }

    @BeforeEach
    public void initTest() {
        equipamento = createEntity(em);
    }

    @Test
    @Transactional
    void createEquipamento() throws Exception {
        int databaseSizeBeforeCreate = equipamentoRepository.findAll().size();
        // Create the Equipamento
        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isCreated());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeCreate + 1);
        Equipamento testEquipamento = equipamentoList.get(equipamentoList.size() - 1);
        assertThat(testEquipamento.getNumeroPatrimonio()).isEqualTo(DEFAULT_NUMERO_PATRIMONIO);
        assertThat(testEquipamento.getNumeroSerie()).isEqualTo(DEFAULT_NUMERO_SERIE);
        assertThat(testEquipamento.getCategoria()).isEqualTo(DEFAULT_CATEGORIA);
        assertThat(testEquipamento.getFabricante()).isEqualTo(DEFAULT_FABRICANTE);
        assertThat(testEquipamento.getModelo()).isEqualTo(DEFAULT_MODELO);
        assertThat(testEquipamento.getDataManutencao()).isEqualTo(DEFAULT_DATA_MANUTENCAO);
        assertThat(testEquipamento.getEstadoConservacao()).isEqualTo(DEFAULT_ESTADO_CONSERVACAO);
        assertThat(testEquipamento.getFuncionando()).isEqualTo(DEFAULT_FUNCIONANDO);
    }

    @Test
    @Transactional
    void createEquipamentoWithExistingId() throws Exception {
        // Create the Equipamento with an existing ID
        equipamento.setId(1L);

        int databaseSizeBeforeCreate = equipamentoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNumeroPatrimonioIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipamentoRepository.findAll().size();
        // set the field null
        equipamento.setNumeroPatrimonio(null);

        // Create the Equipamento, which fails.

        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNumeroSerieIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipamentoRepository.findAll().size();
        // set the field null
        equipamento.setNumeroSerie(null);

        // Create the Equipamento, which fails.

        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCategoriaIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipamentoRepository.findAll().size();
        // set the field null
        equipamento.setCategoria(null);

        // Create the Equipamento, which fails.

        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFabricanteIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipamentoRepository.findAll().size();
        // set the field null
        equipamento.setFabricante(null);

        // Create the Equipamento, which fails.

        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkModeloIsRequired() throws Exception {
        int databaseSizeBeforeTest = equipamentoRepository.findAll().size();
        // set the field null
        equipamento.setModelo(null);

        // Create the Equipamento, which fails.

        restEquipamentoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isBadRequest());

        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEquipamentos() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        // Get all the equipamentoList
        restEquipamentoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(equipamento.getId().intValue())))
            .andExpect(jsonPath("$.[*].numeroPatrimonio").value(hasItem(DEFAULT_NUMERO_PATRIMONIO)))
            .andExpect(jsonPath("$.[*].numeroSerie").value(hasItem(DEFAULT_NUMERO_SERIE)))
            .andExpect(jsonPath("$.[*].categoria").value(hasItem(DEFAULT_CATEGORIA.toString())))
            .andExpect(jsonPath("$.[*].fabricante").value(hasItem(DEFAULT_FABRICANTE)))
            .andExpect(jsonPath("$.[*].modelo").value(hasItem(DEFAULT_MODELO)))
            .andExpect(jsonPath("$.[*].dataManutencao").value(hasItem(DEFAULT_DATA_MANUTENCAO.toString())))
            .andExpect(jsonPath("$.[*].estadoConservacao").value(hasItem(DEFAULT_ESTADO_CONSERVACAO.toString())))
            .andExpect(jsonPath("$.[*].funcionando").value(hasItem(DEFAULT_FUNCIONANDO.booleanValue())));
    }

    @Test
    @Transactional
    void getEquipamento() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        // Get the equipamento
        restEquipamentoMockMvc
            .perform(get(ENTITY_API_URL_ID, equipamento.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(equipamento.getId().intValue()))
            .andExpect(jsonPath("$.numeroPatrimonio").value(DEFAULT_NUMERO_PATRIMONIO))
            .andExpect(jsonPath("$.numeroSerie").value(DEFAULT_NUMERO_SERIE))
            .andExpect(jsonPath("$.categoria").value(DEFAULT_CATEGORIA.toString()))
            .andExpect(jsonPath("$.fabricante").value(DEFAULT_FABRICANTE))
            .andExpect(jsonPath("$.modelo").value(DEFAULT_MODELO))
            .andExpect(jsonPath("$.dataManutencao").value(DEFAULT_DATA_MANUTENCAO.toString()))
            .andExpect(jsonPath("$.estadoConservacao").value(DEFAULT_ESTADO_CONSERVACAO.toString()))
            .andExpect(jsonPath("$.funcionando").value(DEFAULT_FUNCIONANDO.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingEquipamento() throws Exception {
        // Get the equipamento
        restEquipamentoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEquipamento() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();

        // Update the equipamento
        Equipamento updatedEquipamento = equipamentoRepository.findById(equipamento.getId()).get();
        // Disconnect from session so that the updates on updatedEquipamento are not directly saved in db
        em.detach(updatedEquipamento);
        updatedEquipamento
            .numeroPatrimonio(UPDATED_NUMERO_PATRIMONIO)
            .numeroSerie(UPDATED_NUMERO_SERIE)
            .categoria(UPDATED_CATEGORIA)
            .fabricante(UPDATED_FABRICANTE)
            .modelo(UPDATED_MODELO)
            .dataManutencao(UPDATED_DATA_MANUTENCAO)
            .estadoConservacao(UPDATED_ESTADO_CONSERVACAO)
            .funcionando(UPDATED_FUNCIONANDO);

        restEquipamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEquipamento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEquipamento))
            )
            .andExpect(status().isOk());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
        Equipamento testEquipamento = equipamentoList.get(equipamentoList.size() - 1);
        assertThat(testEquipamento.getNumeroPatrimonio()).isEqualTo(UPDATED_NUMERO_PATRIMONIO);
        assertThat(testEquipamento.getNumeroSerie()).isEqualTo(UPDATED_NUMERO_SERIE);
        assertThat(testEquipamento.getCategoria()).isEqualTo(UPDATED_CATEGORIA);
        assertThat(testEquipamento.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testEquipamento.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testEquipamento.getDataManutencao()).isEqualTo(UPDATED_DATA_MANUTENCAO);
        assertThat(testEquipamento.getEstadoConservacao()).isEqualTo(UPDATED_ESTADO_CONSERVACAO);
        assertThat(testEquipamento.getFuncionando()).isEqualTo(UPDATED_FUNCIONANDO);
    }

    @Test
    @Transactional
    void putNonExistingEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, equipamento.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(equipamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(equipamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(equipamento)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEquipamentoWithPatch() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();

        // Update the equipamento using partial update
        Equipamento partialUpdatedEquipamento = new Equipamento();
        partialUpdatedEquipamento.setId(equipamento.getId());

        partialUpdatedEquipamento.fabricante(UPDATED_FABRICANTE).modelo(UPDATED_MODELO).funcionando(UPDATED_FUNCIONANDO);

        restEquipamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEquipamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEquipamento))
            )
            .andExpect(status().isOk());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
        Equipamento testEquipamento = equipamentoList.get(equipamentoList.size() - 1);
        assertThat(testEquipamento.getNumeroPatrimonio()).isEqualTo(DEFAULT_NUMERO_PATRIMONIO);
        assertThat(testEquipamento.getNumeroSerie()).isEqualTo(DEFAULT_NUMERO_SERIE);
        assertThat(testEquipamento.getCategoria()).isEqualTo(DEFAULT_CATEGORIA);
        assertThat(testEquipamento.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testEquipamento.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testEquipamento.getDataManutencao()).isEqualTo(DEFAULT_DATA_MANUTENCAO);
        assertThat(testEquipamento.getEstadoConservacao()).isEqualTo(DEFAULT_ESTADO_CONSERVACAO);
        assertThat(testEquipamento.getFuncionando()).isEqualTo(UPDATED_FUNCIONANDO);
    }

    @Test
    @Transactional
    void fullUpdateEquipamentoWithPatch() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();

        // Update the equipamento using partial update
        Equipamento partialUpdatedEquipamento = new Equipamento();
        partialUpdatedEquipamento.setId(equipamento.getId());

        partialUpdatedEquipamento
            .numeroPatrimonio(UPDATED_NUMERO_PATRIMONIO)
            .numeroSerie(UPDATED_NUMERO_SERIE)
            .categoria(UPDATED_CATEGORIA)
            .fabricante(UPDATED_FABRICANTE)
            .modelo(UPDATED_MODELO)
            .dataManutencao(UPDATED_DATA_MANUTENCAO)
            .estadoConservacao(UPDATED_ESTADO_CONSERVACAO)
            .funcionando(UPDATED_FUNCIONANDO);

        restEquipamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEquipamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEquipamento))
            )
            .andExpect(status().isOk());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
        Equipamento testEquipamento = equipamentoList.get(equipamentoList.size() - 1);
        assertThat(testEquipamento.getNumeroPatrimonio()).isEqualTo(UPDATED_NUMERO_PATRIMONIO);
        assertThat(testEquipamento.getNumeroSerie()).isEqualTo(UPDATED_NUMERO_SERIE);
        assertThat(testEquipamento.getCategoria()).isEqualTo(UPDATED_CATEGORIA);
        assertThat(testEquipamento.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testEquipamento.getModelo()).isEqualTo(UPDATED_MODELO);
        assertThat(testEquipamento.getDataManutencao()).isEqualTo(UPDATED_DATA_MANUTENCAO);
        assertThat(testEquipamento.getEstadoConservacao()).isEqualTo(UPDATED_ESTADO_CONSERVACAO);
        assertThat(testEquipamento.getFuncionando()).isEqualTo(UPDATED_FUNCIONANDO);
    }

    @Test
    @Transactional
    void patchNonExistingEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, equipamento.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(equipamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(equipamento))
            )
            .andExpect(status().isBadRequest());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEquipamento() throws Exception {
        int databaseSizeBeforeUpdate = equipamentoRepository.findAll().size();
        equipamento.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquipamentoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(equipamento))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Equipamento in the database
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEquipamento() throws Exception {
        // Initialize the database
        equipamentoRepository.saveAndFlush(equipamento);

        int databaseSizeBeforeDelete = equipamentoRepository.findAll().size();

        // Delete the equipamento
        restEquipamentoMockMvc
            .perform(delete(ENTITY_API_URL_ID, equipamento.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Equipamento> equipamentoList = equipamentoRepository.findAll();
        assertThat(equipamentoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
