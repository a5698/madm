package br.com.esmeralda.sca.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.esmeralda.sca.IntegrationTest;
import br.com.esmeralda.sca.domain.Responsavel;
import br.com.esmeralda.sca.repository.ResponsavelRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ResponsavelResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ResponsavelResourceIT {

    private static final String DEFAULT_NOME_DEPARTAMENTO = "AAAAAAAAAA";
    private static final String UPDATED_NOME_DEPARTAMENTO = "BBBBBBBBBB";

    private static final String DEFAULT_UTILIZACAO = "AAAAAAAAAA";
    private static final String UPDATED_UTILIZACAO = "BBBBBBBBBB";

    private static final String DEFAULT_NOME_RESPONSAVEL = "AAAAAAAAAA";
    private static final String UPDATED_NOME_RESPONSAVEL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/responsavels";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ResponsavelRepository responsavelRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restResponsavelMockMvc;

    private Responsavel responsavel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Responsavel createEntity(EntityManager em) {
        Responsavel responsavel = new Responsavel()
            .nomeDepartamento(DEFAULT_NOME_DEPARTAMENTO)
            .utilizacao(DEFAULT_UTILIZACAO)
            .nomeResponsavel(DEFAULT_NOME_RESPONSAVEL);
        return responsavel;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Responsavel createUpdatedEntity(EntityManager em) {
        Responsavel responsavel = new Responsavel()
            .nomeDepartamento(UPDATED_NOME_DEPARTAMENTO)
            .utilizacao(UPDATED_UTILIZACAO)
            .nomeResponsavel(UPDATED_NOME_RESPONSAVEL);
        return responsavel;
    }

    @BeforeEach
    public void initTest() {
        responsavel = createEntity(em);
    }

    @Test
    @Transactional
    void createResponsavel() throws Exception {
        int databaseSizeBeforeCreate = responsavelRepository.findAll().size();
        // Create the Responsavel
        restResponsavelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(responsavel)))
            .andExpect(status().isCreated());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeCreate + 1);
        Responsavel testResponsavel = responsavelList.get(responsavelList.size() - 1);
        assertThat(testResponsavel.getNomeDepartamento()).isEqualTo(DEFAULT_NOME_DEPARTAMENTO);
        assertThat(testResponsavel.getUtilizacao()).isEqualTo(DEFAULT_UTILIZACAO);
        assertThat(testResponsavel.getNomeResponsavel()).isEqualTo(DEFAULT_NOME_RESPONSAVEL);
    }

    @Test
    @Transactional
    void createResponsavelWithExistingId() throws Exception {
        // Create the Responsavel with an existing ID
        responsavel.setId(1L);

        int databaseSizeBeforeCreate = responsavelRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restResponsavelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(responsavel)))
            .andExpect(status().isBadRequest());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomeDepartamentoIsRequired() throws Exception {
        int databaseSizeBeforeTest = responsavelRepository.findAll().size();
        // set the field null
        responsavel.setNomeDepartamento(null);

        // Create the Responsavel, which fails.

        restResponsavelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(responsavel)))
            .andExpect(status().isBadRequest());

        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUtilizacaoIsRequired() throws Exception {
        int databaseSizeBeforeTest = responsavelRepository.findAll().size();
        // set the field null
        responsavel.setUtilizacao(null);

        // Create the Responsavel, which fails.

        restResponsavelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(responsavel)))
            .andExpect(status().isBadRequest());

        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllResponsavels() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        // Get all the responsavelList
        restResponsavelMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(responsavel.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomeDepartamento").value(hasItem(DEFAULT_NOME_DEPARTAMENTO)))
            .andExpect(jsonPath("$.[*].utilizacao").value(hasItem(DEFAULT_UTILIZACAO)))
            .andExpect(jsonPath("$.[*].nomeResponsavel").value(hasItem(DEFAULT_NOME_RESPONSAVEL)));
    }

    @Test
    @Transactional
    void getResponsavel() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        // Get the responsavel
        restResponsavelMockMvc
            .perform(get(ENTITY_API_URL_ID, responsavel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(responsavel.getId().intValue()))
            .andExpect(jsonPath("$.nomeDepartamento").value(DEFAULT_NOME_DEPARTAMENTO))
            .andExpect(jsonPath("$.utilizacao").value(DEFAULT_UTILIZACAO))
            .andExpect(jsonPath("$.nomeResponsavel").value(DEFAULT_NOME_RESPONSAVEL));
    }

    @Test
    @Transactional
    void getNonExistingResponsavel() throws Exception {
        // Get the responsavel
        restResponsavelMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewResponsavel() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();

        // Update the responsavel
        Responsavel updatedResponsavel = responsavelRepository.findById(responsavel.getId()).get();
        // Disconnect from session so that the updates on updatedResponsavel are not directly saved in db
        em.detach(updatedResponsavel);
        updatedResponsavel
            .nomeDepartamento(UPDATED_NOME_DEPARTAMENTO)
            .utilizacao(UPDATED_UTILIZACAO)
            .nomeResponsavel(UPDATED_NOME_RESPONSAVEL);

        restResponsavelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedResponsavel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedResponsavel))
            )
            .andExpect(status().isOk());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
        Responsavel testResponsavel = responsavelList.get(responsavelList.size() - 1);
        assertThat(testResponsavel.getNomeDepartamento()).isEqualTo(UPDATED_NOME_DEPARTAMENTO);
        assertThat(testResponsavel.getUtilizacao()).isEqualTo(UPDATED_UTILIZACAO);
        assertThat(testResponsavel.getNomeResponsavel()).isEqualTo(UPDATED_NOME_RESPONSAVEL);
    }

    @Test
    @Transactional
    void putNonExistingResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, responsavel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(responsavel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(responsavel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(responsavel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateResponsavelWithPatch() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();

        // Update the responsavel using partial update
        Responsavel partialUpdatedResponsavel = new Responsavel();
        partialUpdatedResponsavel.setId(responsavel.getId());

        partialUpdatedResponsavel.nomeResponsavel(UPDATED_NOME_RESPONSAVEL);

        restResponsavelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedResponsavel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedResponsavel))
            )
            .andExpect(status().isOk());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
        Responsavel testResponsavel = responsavelList.get(responsavelList.size() - 1);
        assertThat(testResponsavel.getNomeDepartamento()).isEqualTo(DEFAULT_NOME_DEPARTAMENTO);
        assertThat(testResponsavel.getUtilizacao()).isEqualTo(DEFAULT_UTILIZACAO);
        assertThat(testResponsavel.getNomeResponsavel()).isEqualTo(UPDATED_NOME_RESPONSAVEL);
    }

    @Test
    @Transactional
    void fullUpdateResponsavelWithPatch() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();

        // Update the responsavel using partial update
        Responsavel partialUpdatedResponsavel = new Responsavel();
        partialUpdatedResponsavel.setId(responsavel.getId());

        partialUpdatedResponsavel
            .nomeDepartamento(UPDATED_NOME_DEPARTAMENTO)
            .utilizacao(UPDATED_UTILIZACAO)
            .nomeResponsavel(UPDATED_NOME_RESPONSAVEL);

        restResponsavelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedResponsavel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedResponsavel))
            )
            .andExpect(status().isOk());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
        Responsavel testResponsavel = responsavelList.get(responsavelList.size() - 1);
        assertThat(testResponsavel.getNomeDepartamento()).isEqualTo(UPDATED_NOME_DEPARTAMENTO);
        assertThat(testResponsavel.getUtilizacao()).isEqualTo(UPDATED_UTILIZACAO);
        assertThat(testResponsavel.getNomeResponsavel()).isEqualTo(UPDATED_NOME_RESPONSAVEL);
    }

    @Test
    @Transactional
    void patchNonExistingResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, responsavel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(responsavel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(responsavel))
            )
            .andExpect(status().isBadRequest());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamResponsavel() throws Exception {
        int databaseSizeBeforeUpdate = responsavelRepository.findAll().size();
        responsavel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restResponsavelMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(responsavel))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Responsavel in the database
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteResponsavel() throws Exception {
        // Initialize the database
        responsavelRepository.saveAndFlush(responsavel);

        int databaseSizeBeforeDelete = responsavelRepository.findAll().size();

        // Delete the responsavel
        restResponsavelMockMvc
            .perform(delete(ENTITY_API_URL_ID, responsavel.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Responsavel> responsavelList = responsavelRepository.findAll();
        assertThat(responsavelList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
