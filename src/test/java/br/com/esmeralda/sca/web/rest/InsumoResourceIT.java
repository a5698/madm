package br.com.esmeralda.sca.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.esmeralda.sca.IntegrationTest;
import br.com.esmeralda.sca.domain.Insumo;
import br.com.esmeralda.sca.domain.enumeration.TipoInsumo;
import br.com.esmeralda.sca.repository.InsumoRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link InsumoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class InsumoResourceIT {

    private static final String DEFAULT_COD_INSUMO = "AAAAAAAAAA";
    private static final String UPDATED_COD_INSUMO = "BBBBBBBBBB";

    private static final TipoInsumo DEFAULT_TIPO_INSUMO = TipoInsumo.ACIDO;
    private static final TipoInsumo UPDATED_TIPO_INSUMO = TipoInsumo.ADJUVANTE_AGRICOLA;

    private static final String DEFAULT_FABRICANTE = "AAAAAAAAAA";
    private static final String UPDATED_FABRICANTE = "BBBBBBBBBB";

    private static final String DEFAULT_MODELO = "AAAAAAAAAA";
    private static final String UPDATED_MODELO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/insumos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private InsumoRepository insumoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restInsumoMockMvc;

    private Insumo insumo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Insumo createEntity(EntityManager em) {
        Insumo insumo = new Insumo()
            .codInsumo(DEFAULT_COD_INSUMO)
            .tipoInsumo(DEFAULT_TIPO_INSUMO)
            .fabricante(DEFAULT_FABRICANTE)
            .modelo(DEFAULT_MODELO);
        return insumo;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Insumo createUpdatedEntity(EntityManager em) {
        Insumo insumo = new Insumo()
            .codInsumo(UPDATED_COD_INSUMO)
            .tipoInsumo(UPDATED_TIPO_INSUMO)
            .fabricante(UPDATED_FABRICANTE)
            .modelo(UPDATED_MODELO);
        return insumo;
    }

    @BeforeEach
    public void initTest() {
        insumo = createEntity(em);
    }

    @Test
    @Transactional
    void createInsumo() throws Exception {
        int databaseSizeBeforeCreate = insumoRepository.findAll().size();
        // Create the Insumo
        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isCreated());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeCreate + 1);
        Insumo testInsumo = insumoList.get(insumoList.size() - 1);
        assertThat(testInsumo.getCodInsumo()).isEqualTo(DEFAULT_COD_INSUMO);
        assertThat(testInsumo.getTipoInsumo()).isEqualTo(DEFAULT_TIPO_INSUMO);
        assertThat(testInsumo.getFabricante()).isEqualTo(DEFAULT_FABRICANTE);
        assertThat(testInsumo.getModelo()).isEqualTo(DEFAULT_MODELO);
    }

    @Test
    @Transactional
    void createInsumoWithExistingId() throws Exception {
        // Create the Insumo with an existing ID
        insumo.setId(1L);

        int databaseSizeBeforeCreate = insumoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isBadRequest());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCodInsumoIsRequired() throws Exception {
        int databaseSizeBeforeTest = insumoRepository.findAll().size();
        // set the field null
        insumo.setCodInsumo(null);

        // Create the Insumo, which fails.

        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isBadRequest());

        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTipoInsumoIsRequired() throws Exception {
        int databaseSizeBeforeTest = insumoRepository.findAll().size();
        // set the field null
        insumo.setTipoInsumo(null);

        // Create the Insumo, which fails.

        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isBadRequest());

        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFabricanteIsRequired() throws Exception {
        int databaseSizeBeforeTest = insumoRepository.findAll().size();
        // set the field null
        insumo.setFabricante(null);

        // Create the Insumo, which fails.

        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isBadRequest());

        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkModeloIsRequired() throws Exception {
        int databaseSizeBeforeTest = insumoRepository.findAll().size();
        // set the field null
        insumo.setModelo(null);

        // Create the Insumo, which fails.

        restInsumoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isBadRequest());

        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllInsumos() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        // Get all the insumoList
        restInsumoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(insumo.getId().intValue())))
            .andExpect(jsonPath("$.[*].codInsumo").value(hasItem(DEFAULT_COD_INSUMO)))
            .andExpect(jsonPath("$.[*].tipoInsumo").value(hasItem(DEFAULT_TIPO_INSUMO.toString())))
            .andExpect(jsonPath("$.[*].fabricante").value(hasItem(DEFAULT_FABRICANTE)))
            .andExpect(jsonPath("$.[*].modelo").value(hasItem(DEFAULT_MODELO)));
    }

    @Test
    @Transactional
    void getInsumo() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        // Get the insumo
        restInsumoMockMvc
            .perform(get(ENTITY_API_URL_ID, insumo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(insumo.getId().intValue()))
            .andExpect(jsonPath("$.codInsumo").value(DEFAULT_COD_INSUMO))
            .andExpect(jsonPath("$.tipoInsumo").value(DEFAULT_TIPO_INSUMO.toString()))
            .andExpect(jsonPath("$.fabricante").value(DEFAULT_FABRICANTE))
            .andExpect(jsonPath("$.modelo").value(DEFAULT_MODELO));
    }

    @Test
    @Transactional
    void getNonExistingInsumo() throws Exception {
        // Get the insumo
        restInsumoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewInsumo() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();

        // Update the insumo
        Insumo updatedInsumo = insumoRepository.findById(insumo.getId()).get();
        // Disconnect from session so that the updates on updatedInsumo are not directly saved in db
        em.detach(updatedInsumo);
        updatedInsumo.codInsumo(UPDATED_COD_INSUMO).tipoInsumo(UPDATED_TIPO_INSUMO).fabricante(UPDATED_FABRICANTE).modelo(UPDATED_MODELO);

        restInsumoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedInsumo.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedInsumo))
            )
            .andExpect(status().isOk());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
        Insumo testInsumo = insumoList.get(insumoList.size() - 1);
        assertThat(testInsumo.getCodInsumo()).isEqualTo(UPDATED_COD_INSUMO);
        assertThat(testInsumo.getTipoInsumo()).isEqualTo(UPDATED_TIPO_INSUMO);
        assertThat(testInsumo.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testInsumo.getModelo()).isEqualTo(UPDATED_MODELO);
    }

    @Test
    @Transactional
    void putNonExistingInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, insumo.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(insumo))
            )
            .andExpect(status().isBadRequest());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(insumo))
            )
            .andExpect(status().isBadRequest());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateInsumoWithPatch() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();

        // Update the insumo using partial update
        Insumo partialUpdatedInsumo = new Insumo();
        partialUpdatedInsumo.setId(insumo.getId());

        partialUpdatedInsumo.codInsumo(UPDATED_COD_INSUMO).modelo(UPDATED_MODELO);

        restInsumoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInsumo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInsumo))
            )
            .andExpect(status().isOk());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
        Insumo testInsumo = insumoList.get(insumoList.size() - 1);
        assertThat(testInsumo.getCodInsumo()).isEqualTo(UPDATED_COD_INSUMO);
        assertThat(testInsumo.getTipoInsumo()).isEqualTo(DEFAULT_TIPO_INSUMO);
        assertThat(testInsumo.getFabricante()).isEqualTo(DEFAULT_FABRICANTE);
        assertThat(testInsumo.getModelo()).isEqualTo(UPDATED_MODELO);
    }

    @Test
    @Transactional
    void fullUpdateInsumoWithPatch() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();

        // Update the insumo using partial update
        Insumo partialUpdatedInsumo = new Insumo();
        partialUpdatedInsumo.setId(insumo.getId());

        partialUpdatedInsumo
            .codInsumo(UPDATED_COD_INSUMO)
            .tipoInsumo(UPDATED_TIPO_INSUMO)
            .fabricante(UPDATED_FABRICANTE)
            .modelo(UPDATED_MODELO);

        restInsumoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedInsumo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedInsumo))
            )
            .andExpect(status().isOk());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
        Insumo testInsumo = insumoList.get(insumoList.size() - 1);
        assertThat(testInsumo.getCodInsumo()).isEqualTo(UPDATED_COD_INSUMO);
        assertThat(testInsumo.getTipoInsumo()).isEqualTo(UPDATED_TIPO_INSUMO);
        assertThat(testInsumo.getFabricante()).isEqualTo(UPDATED_FABRICANTE);
        assertThat(testInsumo.getModelo()).isEqualTo(UPDATED_MODELO);
    }

    @Test
    @Transactional
    void patchNonExistingInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, insumo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(insumo))
            )
            .andExpect(status().isBadRequest());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(insumo))
            )
            .andExpect(status().isBadRequest());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamInsumo() throws Exception {
        int databaseSizeBeforeUpdate = insumoRepository.findAll().size();
        insumo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restInsumoMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(insumo)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Insumo in the database
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteInsumo() throws Exception {
        // Initialize the database
        insumoRepository.saveAndFlush(insumo);

        int databaseSizeBeforeDelete = insumoRepository.findAll().size();

        // Delete the insumo
        restInsumoMockMvc
            .perform(delete(ENTITY_API_URL_ID, insumo.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Insumo> insumoList = insumoRepository.findAll();
        assertThat(insumoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
